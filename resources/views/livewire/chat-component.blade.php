<div>
    <ul>
        @foreach($conversation as $item)
            <div class="mb-2 rounded-lg p-4 w-5/12 {{ $item['username'] === auth()->user()->name ? 'text-right bg-violet-500 ms-auto' : 'bg-fuchsia-400'}}">
                <li>
                    {{ $item['username'] }}: {{ $item['content'] }}
                </li>
            </div>
        @endforeach
    </ul>

    <form wire:submit="submitMessage" class="mt-5 flex justify-between">
        <x-input @class(['w-11/12']) wire:model="content" />
        <x-button>
            Send
        </x-button>
    </form>
</div>
