<?php

namespace App\Livewire;

use App\Models\Message;
use Livewire\Component;
use Livewire\Attributes\On;
use App\Events\MessageEvent;

class ChatComponent extends Component
{
    public $content;
    public $conversation = [];

    public function submitMessage()
    {
        MessageEvent::dispatch(auth()->user()->id, $this->content);
        $this->content = '';
    }

    #[On('echo:chat-channel,MessageEvent')]
    public function listenForMessage($data)
    {
        $this->conversation[] = [
            'username' => $data['username']['name'],
            'content' => $data['content'],
        ];
    }

    public function mount()
    {
        $messages = Message::all();

        foreach($messages as $message){
            $this->conversation[] = [
                'username' => $message->user->name,
                'content' => $message->content,
            ];
        }
    }

    public function render()
    {
        return view('livewire.chat-component');
    }
}
